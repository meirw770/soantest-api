Projet de test pour API Plateform.

Deux environments Master et Test (Prod et PProd).

(Projet réalisé sur une OS Linux)
installations des services nécessaires pour l'environment API Plateform
  (les services doivent être installés avec leurs dépendances)(via apt-get ou yum)
  - Apache2
  - PHP 7.4 (or latest )
  - Symfony (php-symfony)
  - (via la commande wget https://get.symfony.com/cli/installer -O - | bash(bien télécharger dans
    le bon dossier /usr/local/bin/symfony))
  - Composer
  - Text editor (Atom ou autre pour pouvoir faire les modifications et les commit sur GIT)

(il est nécessaire d'avoir des comptes Github et Gitlab)

après installations des paquets on lance la Plateform
La commande pour lancer API Plateform: symfony server:start

pour accéder via un navigateur à api > 127.0.0.1:8000/api et Symfony > 127.0.0.1:8000

Création du Master sur Github puis import sur Gitlab

Sur la branche Test:
  - Ajout d'une page Test  127.0.0.1:8000/lucky/number (page qui donne un nombre aléatoire)
      - modification du fichier /config/routes.yaml
      - ajouts du dossier et fichier /src/Controller/lucky/number.php
